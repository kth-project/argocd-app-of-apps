# Argocd App Of Apps
Argo CD의 App-of-Apps 패턴은 관리 및 배포의 복잡성을 해결하기 위한 방법으로, 여러 Argo CD 애플리케이션들을 하나의 '부모' 애플리케이션을 통해 관리하는 전략입니다. 이 패턴은 큰 규모의 시스템이나 마이크로서비스 아키텍처를 운영할 때 유용하며, 단일 배포 지점을 통해 전체 시스템의 구성 요소를 일관되게 관리할 수 있게 해줍니다.

App-of-Apps 패턴의 작동 방식
1. 부모 애플리케이션: '부모' 애플리케이션은 다른 '자식' 애플리케이션들을 정의한 Git 리포지토리를 참조합니다. 이 리포지토리에는 자식 애플리케이션들을 배포하기 위한 Helm 차트, Kustomize 파일, 또는 일반적인 Kubernetes YAML 파일이 포함될 수 있습니다.

2. 자식 애플리케이션들: 각각의 자식 애플리케이션은 독립적인 Git 리포지토리를 가질 수 있으며, 자체적인 배포, 업데이트, 롤백 등의 생명주기를 가집니다. 이들은 각기 다른 기능을 수행하는 마이크로서비스 또는 시스템 구성 요소일 수 있습니다.

3. 동기화: 부모 애플리케이션을 통해 자식 애플리케이션들의 상태를 동기화합니다. Argo CD 명령어나 UI를 사용하여 부모 애플리케이션을 Sync하면, 연결된 자식 애플리케이션들도 자동으로 Sync됩니다.

App-of-Apps 패턴의 장점
- 중앙 집중식 관리: 모든 애플리케이션을 한 곳에서 관리할 수 있어, 운영의 복잡성을 줄일 수 있습니다.
- 자동화: 새로운 애플리케이션을 추가하거나 기존 애플리케이션을 업데이트하는 과정을 자동화할 수 있습니다.
- 확장성: 새로운 마이크로서비스나 애플리케이션을 시스템에 추가할 때 간단하게 확장할 수 있습니다.
- 일관성: 모든 애플리케이션의 배포가 일관된 방식으로 이루어져, 버전 관리 및 추적이 용이합니다.

## AS-IS 

![Alt text](image-1.png)

AS-IS 특징 : DEV/STG/PRD Cluster 환경에 Argocd 를 각각 설치하여 사용 


## TO-BE

![Alt text](image-2.png)

TO-BE 특징 : CD Cluster에 APPS 배포시 부모 애플리케이션에서 정의한 Dev/Stg/Prd Cluster에 App-A, App-B, App-C 애플리케이션 배포 


## 1. DEMO 환경 사전 작업 
- Terraform Module을 활용한 인프라 구축 및 EKS 생성(VPC, Subnet, NAT, IGW..)
- Argocd 설치
- Argocd DEV/STG/PRD Cluster 등록
- Nginx-Controller 설치 및 Argocd 연결
- Helm Chart 추출 혹은 생성 하여 Git push
- Custom 된 APP-OF-APPS 패턴 적용(Custom 내용 : K8s Server 등록, git URL, git Branch, 그외 Argocd Option 정의)

## 2. DEMO 환경 구성도
![Alt text](image.png)

## 3. Argocd Project 권한 관리
- Argocd Project 생성
    - App-Apps : APPS 배포만을 위한 프로젝트
    - dev-cluster : DEV 환경에 APP 이 배포될 cluster
    - stg-cluster : STG 환경에 APP 이 배포될 cluster
    - prd-cluster : RPD 환경에 APP 이 배포될 cluster

## 4.Project 권한 할당 
### APP-APPS
- SOURCE REPOSITORIES(GIT URL)
- DESTINATIONS(https://kubernetes.default.svc) Argocd가 설치된 클러스터
- CLUSTER RESOURCE ALLOW LIST (*)

### DEV/STG/PRD Cluster 
- SOURCE REPOSITORIES(GIT URL)
- DESTINATIONS app을 배포할 Cluster Server 지정
- CLUSTER RESOURCE ALLOW LIST (*)

## 5. App-Of-Apps Tree 구조
```
├── app # App 어플리케이션의 루트 디렉토리
│   ├── app-be # App의 백엔드 서비스를 위한 Helm 차트 디렉토리
│   │   ├── Chart.yaml # 백엔드 차트의 메타데이터를 정의하는 파일
│   │   ├── charts # 백엔드 차트의 의존성 차트를 포함할 수 있는 디렉토리
│   │   ├── templates # 백엔드 차트의 Kubernetes 매니페스트 템플릿 파일들
│   │   │   ├── NOTES.txt # 설치 후 사용자에게 표시할 메모
│   │   │   ├── _helpers.tpl # 차트에서 사용하는 템플릿 헬퍼 함수들 정의
│   │   │   ├── deployment.yaml # 백엔드 서비스의 Deployment를 정의
│   │   │   ├── hpa.yaml # Horizontal Pod Autoscaler 설정
│   │   │   ├── ingress.yaml # Ingress 리소스 설정
│   │   │   ├── service.yaml # 백엔드 서비스에 대한 Service 설정
│   │   │   ├── serviceaccount.yaml # 백엔드 서비스를 위한 ServiceAccount 설정
│   │   │   └── tests # 차트의 테스트를 위한 템플릿들 
│   │   │       └── test-connection.yaml # 연결 테스트를 위한 템플릿
│   │   └── values.yaml # 백엔드 차트의 기본 설정 값들
│   ├── app-common # App의 공통 서비스를 위한 Helm 차트 디렉토리
│   │   ├── Chart.yaml 
│   │   ├── charts
│   │   ├── templates
│   │   │   ├── NOTES.txt
│   │   │   ├── _helpers.tpl
│   │   │   ├── deployment.yaml
│   │   │   ├── hpa.yaml
│   │   │   ├── ingress.yaml
│   │   │   ├── service.yaml
│   │   │   ├── serviceaccount.yaml
│   │   │   └── tests
│   │   │       └── test-connection.yaml
│   │   └── values.yaml
│   ├── app-fe # App의 프론트엔드 서비스를 위한 Helm 차트 디렉토리
│   │   ├── Chart.yaml
│   │   ├── charts
│   │   ├── templates
│   │   │   ├── NOTES.txt
│   │   │   ├── _helpers.tpl
│   │   │   ├── deployment.yaml
│   │   │   ├── hpa.yaml
│   │   │   ├── ingress.yaml
│   │   │   ├── service.yaml
│   │   │   ├── serviceaccount.yaml
│   │   │   └── tests
│   │   │       └── test-connection.yaml
│   │   └── values.yaml
│   └── apps # App 부모 차트 디렉토리
│       ├── Chart.yaml # 부모 차트의 메타데이터 정의
│       ├── templates # 부모 차트의 자식 차트를 정의하는 템플릿 파일들
│       │   ├── app-be.yaml # 백엔드 차트를 Argo CD 애플리케이션으로 배포 및 Argocd project, Argocd Option 정의
│       │   ├── app-common.yaml # 공통 차트를 Argo CD 애플리케이션으로 배포 및 Argocd project, Argocd Option 정의
│       │   └── app-fe.yaml # 프론트엔드 차트를 Argo CD 애플리케이션으로 배포 및 Argocd project, Argocd Option 정의
│       └── values.yaml # 배포 대상인 DEV/STG/PRD Cluster Server 정보, GIT URL, GIT Branch 정의
└── app2
    ├── app2-be
    │   ├── Chart.yaml
    │   ├── charts
    │   ├── templates
    │   │   ├── NOTES.txt
    │   │   ├── _helpers.tpl
    │   │   ├── deployment.yaml
    │   │   ├── hpa.yaml
    │   │   ├── ingress.yaml
    │   │   ├── service.yaml
    │   │   ├── serviceaccount.yaml
    │   │   └── tests
    │   │       └── test-connection.yaml
    │   └── values.yaml
    ├── app2-common
    │   ├── Chart.yaml
    │   ├── charts
    │   ├── templates
    │   │   ├── NOTES.txt
    │   │   ├── _helpers.tpl
    │   │   ├── deployment.yaml
    │   │   ├── hpa.yaml
    │   │   ├── ingress.yaml
    │   │   ├── service.yaml
    │   │   ├── serviceaccount.yaml
    │   │   └── tests
    │   │       └── test-connection.yaml
    │   └── values.yaml
    ├── app2-fe
    │   ├── Chart.yaml
    │   ├── charts
    │   ├── templates
    │   │   ├── NOTES.txt
    │   │   ├── _helpers.tpl
    │   │   ├── deployment.yaml
    │   │   ├── hpa.yaml
    │   │   ├── ingress.yaml
    │   │   ├── service.yaml
    │   │   ├── serviceaccount.yaml
    │   │   └── tests
    │   │       └── test-connection.yaml
    │   └── values.yaml
    └── apps
        ├── Chart.yaml
        ├── templates
        │   ├── app2-be.yaml
        │   ├── app2-common.yaml
        │   └── app2-fe.yaml
        └── values.yaml
```